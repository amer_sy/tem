﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSC_inh_polymorphism
{
    class Program
    {
        static void f1()
        {
            A a = new A();
            B b = new B();
            C c = new C();
            D d = new D();


            //a.write();
            //b.write();
            //c.write();
            //d.write();


            //A aa = d;
            //aa.write();



            //((A)d).write();
            //(d as A).write();
            //d.write();


        }

        static void f2()
        {
            A a = new A();
            B b = new B();
            C c = new C();
            D d = new D();

            A[] ar = new A[4];
            ar[0] = a;
            ar[1] = b;
            ar[2] = c;
            ar[3] = d;

            for (int i = 0; i < ar.Length; i++)
            {
                ar[i].write();
            }


            //foreach (var x in ar)
            //{
            //    x.write();
            //}
        }
       
        static void Main(string[] args)
        {
            f2();
            Console.ReadKey();
        }

    }
}
