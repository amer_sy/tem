﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSC_inh_polymorphism
{
    class A 
    {
        public void write()
        {
            Console.WriteLine("write in class A");
        }
    }

    class B : A
    {
        public void write()
        {
            Console.WriteLine("write in class B");
        }
    }

    class C : B
    {
        public void write()
        {
            Console.WriteLine("write in class C");
        }
    }

    class D : C
    {
        public void write()
        {
            Console.WriteLine("write in class D");
        }
    }
}
