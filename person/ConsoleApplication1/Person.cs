﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{

    class Person
    {
        public string fname, lname, mobile;
        //public int bYear;
        bool Married;
        Date birthday;// = new Date();
        Date weddingDate = new Date();

        public Person(string fn = "none", string ln = "none", string mob = "09", int bd = 1 , int bm = 1 ,  int by = 2018 )
        {
            fname = fn;
            lname = ln;
            mobile = mob;
            //bYear = by;
            birthday = new Date(bd,bm,by);
        }

        public void Write()
        {
            //Console.WriteLine("{0} {1}", fname, lname);
            Console.WriteLine(FullName());
            //Console.WriteLine(bYear);
            birthday.write();            
            if(Married)
                weddingDate.write();
            else
                Console.WriteLine("not married");
            Console.WriteLine(mobile);
            Console.WriteLine(age());

        }

        public void Read()
        {
            fname = Console.ReadLine();
            lname = Console.ReadLine();
            mobile = Console.ReadLine();

            //int.TryParse(Console.ReadLine(), out bYear);
            birthday.read();
            weddingDate.read();
        }

        public int age()
        {
            return 2018 - birthday.y;
        }

        public string FullName()
        {
            return fname + ", " + lname;
        }
    }

}
