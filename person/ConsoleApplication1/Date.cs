﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Date
    {
        public int d, m, y;

        public Date(int dd = 1, int mm = 1, int yy = 1)
        {
            d = dd;
            m = mm;
            y = yy;
        }

        public void write()
        {
            Console.WriteLine("{0}/{1}/{2}", d, m, y);
        }
        public void read()
        {
            Console.WriteLine("enter date:");
            int.TryParse(Console.ReadLine(), out d);
            int.TryParse(Console.ReadLine(), out m);
            int.TryParse(Console.ReadLine(), out y);
        }

    }
}
