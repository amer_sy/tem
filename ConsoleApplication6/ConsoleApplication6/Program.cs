﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    class Program
    {
        static void Main(string[] args)
        {
            int x,price;
            Console.WriteLine("please enter Quantity of consumption");
            int.TryParse(Console.ReadLine(), out x);
            if (x<=100)
            {
                price = x * 25;
                Console.WriteLine("price : {0}", price);
            }
            else if (x <= 200)
            {
                price =(x-100)*35;
                Console.WriteLine("price : {0}", price);
            }
            else if (x <= 400)
            {
                price = (x - 200) * 50;
                Console.WriteLine("price : {0}", price);
            }
            else if (x <= 600)
            {
                price = 2500 + 7000 +20000+ (x - 400) * 75;
                Console.WriteLine("price : {0}", price);
            }
            else if (x <= 800)
            {
                price = (x - 600) * 200;
                Console.WriteLine("price : {0}", price);
            }
            else if (x <= 1000)
            {
                price = (x - 800) * 300;
                Console.WriteLine("price : {0}", price);
            }
            else if (x <= 2000)
            {
                price =(x - 1000) * 350;
                Console.WriteLine("price : {0}", price);
            }
            else
            {
                price =(x - 2000) * 700;
                Console.WriteLine("price : {0}", price);
            }
            Console.ReadKey();
        }
    }
}
