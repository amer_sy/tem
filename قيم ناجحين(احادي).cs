﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication15
{
    class Program
    {
        static void array(int[]a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                int.TryParse(Console.ReadLine(), out a[i]);
            }
            Console.WriteLine();
        }
        static void pass(int[]a)
        {
            Console.WriteLine("pass");
            for (int i = 0; i < a.Length; i++)
            {
                if(a[i]>60)
                    Console.WriteLine("({0})",a[i]);
                else
                    Console.WriteLine( a[i]);
                   
            }
        }
        static void Main(string[] args)
        {
            int[] a = new int[5];
            array(a);
            pass(a);
            Console.ReadKey();
        }
    }
}
