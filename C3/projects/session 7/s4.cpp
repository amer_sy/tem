
#include "stdafx.h"
#include <iostream>
#include <string>
using namespace std;

string reverse(string s)
{
	string result = "";
	for (int i = s.length() - 1; i >= 0; i--)
	{
		result += s.at(i);
	}
	return result;
}

int _tmain(int argc, _TCHAR* argv[])
{
	string s = "how are you dear?";
	string result = reverse(s);
	cout << result << endl;
}

