
#include "stdafx.h"
#include <iostream>
#include <string>
using namespace std;

double solve(string s)
{
	double result = 0;
	int op = -1;

	if (s.find("+") != -1)
	{
		int opi = s.find("+");

		int n1 = stoi(s.substr(0, opi ));
		int n2 = stoi(s.substr(opi+1 , s.length()-opi ));
		result = n1 + n2;
		return result;
	}
	else if (s.find("-") != -1)
	{
		int opi = s.find("-");
		int n1 = stoi(s.substr(0, opi));
		int n2 = stoi(s.substr(opi + 1, s.length() - opi));
		result = n1 - n2;
		return result;
	}
	else if (s.find("*") != -1)
	{
		int opi = s.find("*");
		int n1 = stoi(s.substr(0, opi));
		int n2 = stoi(s.substr(opi + 1, s.length() - opi));
		result = n1 * n2;
		return result;
	}
	else if (s.find("/") != -1)
	{
		int opi = s.find("/");
		int n1 = stoi(s.substr(0, opi));
		int n2 = stoi(s.substr(opi + 1, s.length() - opi));
		result = n1 / n2;
		return result;
	}

	cout << "error" << endl;
	return result;
}

int _tmain(int argc, _TCHAR* argv[])
{
	string s; // 20+80
	cin >> s;

	double result = solve(s);
	cout << result << endl;
}

