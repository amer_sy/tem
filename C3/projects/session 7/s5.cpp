
#include "stdafx.h"
#include <iostream>
#include <string>
using namespace std;

double solve(string s)
{
	double result = 0;
	char op = s.at(2);

	int n1, n2;
	n1 = stoi(s.substr(0, 2));
	n2 = stoi(s.substr(3, 2));
	
	if (op == '+')
		result = n1 + n2;
	if (op == '-')
		result = n1 - n2;
	if (op == '*')
		result = n1 * n2;
	if (op == '/')
		result = n1 / n2;

	return result;
}

int _tmain(int argc, _TCHAR* argv[])
{
	string s; // 20+80
	cin >> s;

	double result = solve(s);
	cout << result << endl;
}

