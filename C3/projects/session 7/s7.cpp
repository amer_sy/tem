
#include "stdafx.h"
#include <iostream>
#include <string>
#include <Windows.h>	
using namespace std;

double solve(string s , int &color)
{
	double result = 0;
	int op = -1;

	if (s.find("+") != -1)
	{
		int opi = s.find("+");
		color = 11;
		int n1 = stoi(s.substr(0, opi ));
		int n2 = stoi(s.substr(opi+1 , s.length()-opi ));
		result = n1 + n2;
		return result;
	}
	else if (s.find("-") != -1)
	{
		int opi = s.find("-");
		color = 12;
		int n1 = stoi(s.substr(0, opi));
		int n2 = stoi(s.substr(opi + 1, s.length() - opi));
		result = n1 - n2;
		return result;
	}
	else if (s.find("*") != -1)
	{
		int opi = s.find("*");
		color = 13;
		int n1 = stoi(s.substr(0, opi));
		int n2 = stoi(s.substr(opi + 1, s.length() - opi));
		result = n1 * n2;
		return result;
	}
	else if (s.find("/") != -1)
	{
		int opi = s.find("/");
		color = 14;
		int n1 = stoi(s.substr(0, opi));
		int n2 = stoi(s.substr(opi + 1, s.length() - opi));
		result = n1 / n2;
		return result;
	}

	cout << "error" << endl;
	return result;
}

int _tmain(int argc, _TCHAR* argv[])
{
	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(h, 15);

	while (1)
	{
		string s;
		cin >> s;
		int color = 1;

		double result = solve(s, color);
		SetConsoleTextAttribute(h, color);
		cout << result << endl;
		SetConsoleTextAttribute(h, 15);

	}
}

