
#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int is_exist(string sname, string filename)
{
	string line;
	ifstream myfile;
	myfile.open(filename);
	int line_number = 0;

	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			line_number++;
			int comma_position = line.find(",");
			string line_name = line.substr(0, comma_position);
			if (line_name == sname)
				return line_number;
		}

		myfile.close();

		return -1;
	}
	else
	{
		cout << "Unable to open file";
		return 0;
	}
}

void main() {

	string filename = "students.txt";
	cout << "Enter student name to find : " << endl;
	string sname;
	cin >> sname;
	cout << is_exist(sname, filename) << endl;

}