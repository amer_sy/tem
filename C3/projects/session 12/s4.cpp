
#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

void write_file_to_file(string input_name, string output_name , string sname , float newavg)
{

	ifstream input_file;
	ofstream output_file;

	input_file.open(input_name);
	output_file.open(output_name);

	output_file << "-------- START ------\n";

	string line;
	
	if (input_file.is_open())
	{
		while (getline(input_file, line))
		{
			int comma1_position = line.find(",");
			string ids = line.substr(0, comma1_position);

			string line2 = line.substr(comma1_position + 1 );

			int comma2_position = line2.find(",");
			string line_name = line2.substr(0, comma2_position);

			if (line_name == sname)
			{
				output_file << ids << "," << sname << "," << newavg << endl;
			}
			else
				output_file << line << endl;
		}
	}

	else
		cout << "Unable to open file";

	output_file << "-------- END ------\n";

	input_file.close();
	output_file.close();

}

void main() {
	string input_txt = "students.txt";
	string output_txt = "new_studewnts.txt";

	cout << "Enter student name then new average" << endl;
	string sname;
	float avg;
	cin >> sname >> avg;

	write_file_to_file(input_txt, output_txt , sname , avg);
}