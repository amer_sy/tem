#include "graphics.h"

void draw_car(int x0 , int y0)
{
	setcolor(6);

	circle(x0 + 100,285,15); 
	circle(x0 + 200,285,15);
	circle(x0 + 100,285,5);  
	circle(x0 + 200,285,5);

	setcolor(7);

	line(x0 + 65,285,x0 + 85,285);
	line(x0 + 115,285,x0 + 185,285);
	line(x0 + 215,285,x0 + 235,285);
	line(x0 + 65,285,x0 + 65,260);
	line(x0 + 235,285,x0 + 235,260);

	line(x0 + 65,260,x0 + 100,255);
	line(x0 + 235,260,x0 + 200,255);

	line(x0 + 100,255,x0 + 115,235);
	line(x0 + 200,255,x0 + 185,235);

	line(x0 + 115,235,x0 + 185,235);

	line(x0 + 106,255,x0 + 118,238);
	line(x0 + 118,238,x0 + 118,255);
	line(x0 + 106,255,x0 + 118,255);

	line(x0 + 194,255,x0 + 182,238);
	line(x0 + 182,238,x0 + 182,255);
	line(x0 + 194,255,x0 + 182,255);

	line(x0 + 121,238,x0 + 121,255);
	line(x0 + 121,238,x0 + 148,238);
	line(x0 + 121,255,x0 + 148,255);
	line(x0 + 148,255,x0 + 148,238);

	line(x0 + 179,238,x0 + 179,255);
	line(x0 + 179,238,x0 + 152,238);
	line(x0 + 179,255,x0 + 152,255);
	line(x0 + 152,255,x0 + 152,238);
}


/*


cleardevice();
delay(50);
*/


void main()
{
	int gd = DETECT, gm;
	initgraph(&gd, &gm,NULL);
	setcolor(7);

	int x0 = 50;
	int y0 = 300;
	int r = 35;
	int len = 150;

	for (x0 = 50; x0 <= 250; x0 = x0 + 5)
	{

		cleardevice();
		line(485, 0, 485, 500);
		draw_car(x0, y0);
		delay(50);
	}

	line(485, 0, 485, 300);
	line(485, 260, 500, 260);
	line(485, 285, 500, 285);
	line(500, 260, 500, 285);


	getch();
	closegraph();
}
