
#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

void main() {
	string line;
	ifstream myfile;
	myfile.open("students.txt");
	
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			cout << line << endl;
			getchar();
		}
		myfile.close();
	}
	else
		cout << "Unable to open file";

}