
#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

struct student
{
	string name;
	float average;
};

const int n = 9; //  get_lines_number("students.txt");

int get_lines_number(string file_name_txt)
{
	ifstream myfile;
	myfile.open(file_name_txt);
	int counter = 0;
	string line;

	if (myfile.is_open())
		while (getline(myfile, line))
			counter++;
	
	return counter;
	myfile.close();
}

void save_file_to_array(student s[n] , string file_name_txt)
{
	ifstream myfile;
	string line;
	int index = 0;

	myfile.open(file_name_txt);
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			int semi = line.find(",");

			string line_name = line.substr(0, semi);
			int line_avg = stoi(line.substr(semi + 1, line.length() - semi));

			s[index].name = line_name;
			s[index].average = line_avg;

			index++;
		}
	}
	else
		cout << "Unable to open file";
}

void print_maxs(student s[n])
{

}

void main() {

	student s[n];
	save_file_to_array(s, "students.txt");
	print_maxs(s);

}