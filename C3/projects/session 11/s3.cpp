
#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

void main() {
	string line;
	ifstream myfile;
	int max_avg = -1;
	string max_name;

	myfile.open("students.txt");
	
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			cout << line << endl;
			int semi = line.find(",");
			int average = stoi(line.substr(semi + 1, line.length() - semi));
			if (average > max_avg)
			{
				max_avg = average;
				max_name = line.substr(0,semi);
			}

		}
		myfile.close();
		cout << "-------------------\n";
		cout << "max : " << max_name << "," << max_avg << endl;
		cout << "-------------------\n";

	}
	else
		cout << "Unable to open file";

}