// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
using namespace std;

struct student
{
	string first_name;
	string last_name;
	string gender;
	string date_of_birth;
	double average;
};

void main(int argc, _TCHAR* argv[])
{
	const int n = 4;
	student s[n];
	
	for (int i = 0; i < n; i++)
	{
		cout << "First name : ";
		cin >> s[i].first_name;

		cout << "Last name : ";
		cin >> s[i].last_name;

		cout << "Gender : ";
		cin >> s[i].gender;

		cout << "Date of Birth : ";
		cin >> s[i].date_of_birth;

		cout << "Average : ";
		cin >> s[i].average;
	}


}

void sort_array()
{
	const int n = 10;
	int a[n];
	for (int i = 0; i < n - 1; i++)
		for (int j = i + 1; j < n; j++)
			if (a[j] > a[i])
			{
				int temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
}
