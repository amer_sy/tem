
#include "stdafx.h"
#include <iostream>
#include <string>
using namespace std;

const int n = 4;

struct student
{
	string first_name;
	string last_name;
	/*string gender;
	string date_of_birth;*/
	double average;
};

void enter_array(student s[n])
{
	for (int i = 0; i < n; i++)
	{
		cout << i<< " First name : ";
		cin >> s[i].first_name;

		cout << i<< " Last name : ";
		cin >> s[i].last_name;

		/*cout << "Gender : ";
		cin >> s[i].gender;

		cout << "Date of Birth : ";
		cin >> s[i].date_of_birth;*/

		cout << i<< " Average : ";
		cin >> s[i].average;
		cout << "------------------------------------" << endl;
	}
}

void sort_array(student s[n])
{
	for (int i = 0; i < n - 1; i++)
		for (int j = i + 1; j < n; j++)
			if (s[j].average > s[i].average)
			{
				student temp = s[i];
				s[i] = s[j];
				s[j] = temp;
			}
}

void print_array(student s[n])
{
	cout << "------------------------------------" << endl;

	for (int i = 0; i < n; i++)
	{
		cout << "student : " << i << "-) First name : " << s[i].first_name << endl;
		cout << "student : " << i << "-) Last name : " << s[i].last_name << endl;
		cout << "student : " << i << "-) Average : " << s[i].average << endl;
		cout << "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+" << endl;
	}
	cout << "------------------------------------" << endl;

}

void print_max_average(student s[n])
{
	//cout << s[0].first_name << " , " << s[0].average << endl;

	cout << "------------------------------------" << endl;

	cout << "Max average students : " << endl;

	double max = s[0].average;
	for (int i = 0; i < n; i++)
		if (s[i].average == max)
			cout << s[i].first_name << " , " << s[i].average << endl;
		else
			break;
}

void main(int argc, _TCHAR* argv[])
{
	student s[n];

	enter_array(s);

	print_array(s);

	sort_array(s);

	print_array(s);

	print_max_average(s);
}

