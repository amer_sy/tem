
#include "stdafx.h"
#include <iostream>
#include <Windows.h>
#include <iomanip>

using namespace std;

void main()
{
	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(h, 15);

	srand((time(NULL) * 1250));

	int number;
	int sum = 0;

	for (int i = 1; i < 100; i++)
	{
		number = rand() % 10;
		if (number == 1)
		{
			sum++;
			SetConsoleTextAttribute(h, 14);
		}
		else
			SetConsoleTextAttribute(h, 13);

		cout << number;

		SetConsoleTextAttribute(h, 15);

		cout << " , ";

		Sleep(30);
	}

	cout << endl << sum << endl;
}

