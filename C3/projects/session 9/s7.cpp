
#include "stdafx.h"
#include <iostream>
#include <Windows.h>
#include <iomanip>
#include <string>

using namespace std;

string random_password(int n)
{
	string pass = "";

	for (int i = 1; i <= n; i++)
	{
		int number = (rand() % 10); // Random number from 0 to 9
		pass += to_string(number);
	}

	return pass;
}

void main()
{
	srand((time(NULL) * 1250));

	int number , n;
	cout << "Enter n : \n";
	cin >> n;
	
	cout << random_password(n) << endl;


}

