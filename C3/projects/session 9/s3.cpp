#include "stdafx.h"
#include <iostream>
using namespace std;

void main() {

	int *p1, *p2;

	p1 = new int();
	p2 = new int();

	cout << "Enter Two numbers \n";
	cin >> *p1 >> *p2;

	int *p;
	p = new int();

	*p = (*p1) + (*p2) ;
	cout << "+ : " << *p << endl;

	p1 = p2 = p = NULL;
	delete p1, p2, p;

	//cout << *p;  // error
	//cout << p;   // error


}