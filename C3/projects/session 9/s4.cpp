
#include "stdafx.h"
#include <iostream>

using namespace std;

void sort3(int *n1, int *n2, int *n3)
{
	if (*n1 < *n2 && *n1 < *n3)
		if (*n2 < *n3)
			cout << *n1 << " , " << *n2 << " , " << *n3 << endl;
		else
			cout << *n1 << " , " << *n3 << " , " << *n2 << endl;
	else if (*n2 < *n1 && *n2 < *n3)
		if (*n1 < *n3)
			cout << *n2 << " , " << *n1 << " , " << *n3 << endl;
		else
			cout << *n2 << " , " << *n3 << " , " << *n1 << endl;
	else if (*n3 < *n1 && *n3 < *n2)
		if (*n1 < *n2)
			cout << *n3 << " , " << *n1 << " , " << *n2 << endl;
		else
			cout << *n3 << " , " << *n2 << " , " << *n1 << endl;
}

void main() {
	int *p1, *p2 , *p3;

	p1 = new int();
	p2 = new int();
	p3 = new int();

	cout << "Enter three numbers \n";
	cin >> *p1 >> *p2 >> *p3;

	sort3(p1, p2, p3);

	p1 = p2 = p3 = NULL;
	delete p1, p2, p3;

}