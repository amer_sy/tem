
#include "stdafx.h"
#include <iostream>
#include <Windows.h>
#include <iomanip>

using namespace std;

void main()
{
	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(h, 15);

	srand((time(NULL) * 1250));

	int number;
	int sum = 0;

	int pos[10];

	for (int i = 0; i < 10; i++)
		pos[i] = 0;

	for (int i = 1; i <= 1000; i++)
	{
		number = (rand() % 10 ) + 1; // Random number from 1 to 10

		pos[number-1]++;

		SetConsoleTextAttribute(h, number + 4);
		cout << number;

		SetConsoleTextAttribute(h, 15);
		cout << " , ";

		//Sleep(5);
	}

	cout << endl << endl;

	for (int i = 1; i <= 10; i++)
	{
		cout << i << " : " << pos[i-1]/10 << "%" << endl;
	}

}

