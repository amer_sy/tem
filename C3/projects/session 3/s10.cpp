
#include "stdafx.h"
#include <iostream>
#include <Windows.h>

using namespace std;

void main()
{
	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	
	for (int i = 0; i <= 100; i++)
	{
		SetConsoleTextAttribute(h, i);
		cout << i << " : colored sentence" << endl;
	}
	
}

