

#include "stdafx.h"
#include <iostream>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	const int n = 5 ;
	int a[n];

	for (int i = 0; i < n; i++)
		cin >> a[i];

	int min = a[0];
	for (int i = 1; i < n; i++)
		if (a[i] > min)
			min = a[i];
	cout << "max : " << min << endl;

	return 0;
}

