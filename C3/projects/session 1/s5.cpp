
#include "stdafx.h"
#include <iostream>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	int x = 5;
	int y = 10;
	int z = 4;

	x = y++ - 2;
	y = ++x;
	z = x++;

	cout << "x : " << x << endl;
	cout << "y : " << y << endl;
	cout << "z : " << z << endl;

	return 0;
}

