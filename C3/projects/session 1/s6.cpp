
#include "stdafx.h"
#include <iostream>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	int x = 3;
	int y = 6;
	int z = 9;

	x = ++x - y++;
	y = ++z + y;
	x++;
	++z;
	z = z++ - x;

	cout << "x : " << x << endl;
	cout << "y : " << y << endl;
	cout << "z : " << z << endl;

	return 0;
}

