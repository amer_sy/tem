
#include "stdafx.h"
#include <iostream>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	int a = 1;
	int b = 2;
	int c = 3;
	int d = 4;

	a++;
	c++;
	a = b++ - ++a; // a++ ; a = b - a ; b++
	b = c++ - d;
	c = b++ - d++;
	d = c-- + b++; // d = c + b; c-- ; b++;
	--d;

	cout << "a : " << a << endl;
	cout << "b : " << b << endl;
	cout << "c : " << c << endl;
	cout << "d : " << d << endl;

	return 0;
}

