
#include "stdafx.h"
#include <iostream>
#include <math.h>
using namespace std;

class rectangle
{
private:
	double width;
	double length;
public:
	rectangle(){ width = 0; length = 0; };
	rectangle(double w, double l);
	void setWidth(double);
	void setLength(double);
	double getWidth() { return width; }
	double getLength() { return length; }
	double getArea();
	double getCir();
	~rectangle(){/* free memory  */};

};

rectangle::rectangle(double w, double l)
{
	width = w;
	length = l;
};

void rectangle::setWidth(double w)
{
	width = w;
}

void rectangle::setLength(double l)
{
	length = l;
}

double rectangle::getArea()
{
	return width * length;
}

double rectangle::getCir()
{
	return (length + width) * 2;
}

void main()
{
	/*
	rectangle r1;
	double w,l;

	cout << "Enter width then length: ";
	cin >> w>>l;
	r1.setWidth(w);
	r1.setLength(l);

	cout << "Area : " << r1.getArea() << endl;
	cout << "Circumference : " << r1.getCir() << endl;
	*/

	rectangle r2;
	rectangle r3(2,5);

	cout << "default constructor \n";
	cout << r2.getWidth() << endl;
	cout << r2.getLength() << endl;

	cout << "constructor with arguments \n";
	cout << r3.getWidth() << endl;
	cout << r3.getLength() << endl;

}

