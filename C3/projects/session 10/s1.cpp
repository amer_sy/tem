
#include "stdafx.h"
#include <iostream>
#include <math.h>
using namespace std;

class circle
{
private:
	double radius;
public:
	void setRadius(double r);
	double getRadius();
	double getArea();
	double getCir();
};

void circle::setRadius(double r)
{
	radius = r;
}

double circle::getRadius()
{
	return radius;
}

double circle::getArea()
{
	return 3.14 * pow(radius, 2);
}

double circle::getCir()
{
	return 2 * 3.14 * radius;
}

void main()
{
	circle c1 , c2;
	double d;
	cout << "Enter Radius: ";
	cin >> d;
	c1.setRadius(d);

	cout << "Area : " << c1.getArea() << endl;
	cout << "Circumference : " << c1.getCir() << endl;

}

