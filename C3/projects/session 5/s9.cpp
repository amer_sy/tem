// تابع يأخذ وسيط عدد صحيح ويرجع مربع هذا العدد

#include "stdafx.h"
#include <iostream>
using namespace std;

int pow2(int n)
{
	int r = n * n;
	return r;
}

void main(int argc, _TCHAR* argv[])
{
	int n;
	cin >> n;
	int res = pow2(n);
	cout << "result : " << res << endl;
}

