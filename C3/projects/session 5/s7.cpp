// تابع يقوم بحساب عاملي عدد ويطبع الناتج على الشاشة

#include "stdafx.h"
#include <iostream>
using namespace std;

void factorial(int n)
{
	int result = 1;
	for (int i = 2; i <= n; i++)
		result *= i;
	cout << "the result is : " << result << endl;
}

void main(int argc, _TCHAR* argv[])
{
	int n;
	cin >> n;
	factorial(n);
}

