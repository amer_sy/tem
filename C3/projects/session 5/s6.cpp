// حساب عاملي للاعداد من واحد الى عشرة

#include "stdafx.h"
#include <iostream>
using namespace std;

void factorial(int n)
{
	int result = 1;
	for (int i = 2; i <= n; i++)
		result *= i;
	cout << "the result is : " << result << endl;
}

void main(int argc, _TCHAR* argv[])
{
	for (int i = 1; i <= 10; i++)
	{
		cout << i << " : ";
		factorial(i);
	}
}

