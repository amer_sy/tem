// برنامج يقوم بحساب مجموع مربعي عددين باستخدام التوابع
// التابع الأول يقوم بحساب مربع عدد يأخذه كوسيط
// التابع الثاني يقوم بحساب مجموع مربعي عددين يدخلان كوسطاء للتابع

#include "stdafx.h"
#include <iostream>
using namespace std;

int pow2(int n)
{
	int r = n * n;
	return r;
}

int sump2(int s, int x)
{
	int u1 = pow2(s);
	int u2 = pow2(x);
	int u = u1 + u2;
	return u;
}

void main(int argc, _TCHAR* argv[])
{
	int a1 , a2;
	cin >> a1 >> a2;
	int res = sump2(a1 , a2);
	cout << "result : " << res << endl;
}

