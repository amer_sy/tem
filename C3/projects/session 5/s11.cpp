// برنامج يقوم بحساب مجموع مربعي عددين باستخدام التوابع
// التابع الأول يقوم بحساب مربع عدد يأخذه كوسيط
// التابع الثاني يقوم بحساب مجموع مربعي عددين يدخلان كوسطاء للتابع
// طريقة ثانية لحل المسألة

#include "stdafx.h"
#include <iostream>
using namespace std;

int pow2(int n)
{
	int r = n * n;
	return r;
}

int sump2(int s, int x)
{
	return pow2(s) + pow2(x);
}

void main(int argc, _TCHAR* argv[])
{
	int a1, a2;
	cin >> a1 >> a2;
	cout << "result : " << sump2(a1, a2) << endl;
}

