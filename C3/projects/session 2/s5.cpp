
#include "stdafx.h"
#include <iostream>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	int day;
	cout << "Enter day number \n";
	cin >> day;

	if (day == 1)
		cout << "Sat";
	else if (day == 2)
		cout << "Sun";
	else if (day == 3)
		cout << "Mon";
	else if (day == 4)
		cout << "Tue";
	else if (day == 5)
		cout << "Wed";
	else if (day == 6)
		cout << "Thu";
	else if (day == 7)
		cout << "Fri";
	else
		cout << "Error";

	cout << endl;
	return 0;
}

