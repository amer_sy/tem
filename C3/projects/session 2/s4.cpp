
#include "stdafx.h"
#include <iostream>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	double a, b, c;

	cout << "Enter a , b , c \n";
	cin >> a >> b >> c;

	double x1, x2 , sd;

	double d = pow(b, 2) - (4 * a*c);

	if (d < 0)
	{
		cout << "No solutions..." << endl;
	}
	else if (d == 0)
	{
		cout << "There is one doubled solution \n";
		x1 = (-b) / (2 * a);
		cout << "x1 = x2 = " << x1 << endl;
	}
	else // (d > 0)
	{
		cout << "there are two solutions \n";
		sd = sqrt(d);
		x1 = (-b - sd) / (2 * a);
		x2 = (-b + sd) / (2 * a);
		cout << "x1 = " << x1 << endl;
		cout << "x2 = " << x2 << endl;
	}

	return 0;
}

