
#include "stdafx.h"
#include <iostream>
using namespace std;

void main()
{
	const int n = 6;
	const int m = 4;
	int a[n][m];

	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			cin >> a[i][j];

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
			cout << a[i][j] << " , ";
		cout << endl;
	}

}

