

#include "stdafx.h"
#include <iostream>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	const int n = 5;
	int a1[n];
	int a2[n];
	int a3[n];

	for (int i = 0; i < n; i++)
		cin >> a1[i];

	for (int i = 0; i < n; i++)
		cin >> a2[i];

	for (int i = 0; i < n; i++)
		if (a1[i] > a2[i])
			a3[i] = a1[i];
		else
			a3[i] = a2[i];

	for (int i = 0; i < n; i++)
		cout << a1[i] << " , ";
	cout << endl;

	for (int i = 0; i < n; i++)
		cout << a2[i] << " , ";
	cout << endl;

	for (int i = 0; i < n; i++)
		cout << a3[i] << " , ";
	cout << endl;

	return 0;
}

