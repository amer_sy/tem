
#include "stdafx.h"
#include <iostream>
using namespace std;

void main()
{
	const int n = 4;
	const int m = 3;
	int a[n][m];

	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
		{
			cout << "enter a[" << i << "][" << j << "] : ";
			cin >> a[i][j];
		}

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
			cout << a[i][j] << " , ";
		cout << endl;
	}

	for (int i = 0; i < n; i++)
	{
		int max = a[i][0];
		for (int j = 1; j < m; j++)
			if (a[i][j] > max)
				max = a[i][j];
		cout << "max in row " << i << " is : " << max << endl;
	}

}

