

#include "stdafx.h"
#include <iostream>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	const int n = 5;
	int a[n];
	int sum = 0;

	for (int i = 0; i < n; i++)
		cin >> a[i];

	for (int i = 0; i < n; i = i + 2)
		sum += a[i];

	cout << "the sum of even indexes is : " << sum << endl;

	return 0;
}

