

#include "stdafx.h"
#include <iostream>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	const int n = 6;
	int a[n];
	
	for (int i = 0; i < n; i++)
		cin >> a[i];

	int number;
	cout << "Enter the number you want to find \n";
	cin >> number;
	int counter = 0;

	for (int i = 0; i < n; i++)
		if (a[i] == number)
		{
			counter++;
			cout << "index : " << i << endl;
		}

	cout << counter << " times." << endl;

	return 0;
}

