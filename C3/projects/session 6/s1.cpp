
#include "stdafx.h"
#include <iomanip>
#include <string>
#include <iostream>
#include <Windows.h>	
using namespace std;

void print_table(int rownum, int width, int colums)
{
	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(h, 15);

	for (int i = 0; i <= rownum; i++)
	{
		for (int k = 1; k <= colums; k++)
		{
			for (int j = 1; j <= width; j++)
			{
				cout << "-";
			}
			cout << "|";
		}
		cout << endl; 

		if (i != rownum)
		{
			for (int k = 1; k <= colums; k++)
			{
				cout <<setw(width) << "hi";
				cout << "|";
			}
			cout << endl;
		}
	}
}

void main(int argc, _TCHAR* argv[])
{
	
	int r, w, c;

	cout << "Rows : ";
	cin >> r;
	cout << "Width : ";
	cin >> w;
	cout << "Columns : ";
	cin >> c;

	print_table(r, w, c);
	
	
}

