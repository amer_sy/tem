
#include "stdafx.h"
#include <iomanip>
#include <string>
#include <iostream>
#include <Windows.h>	
using namespace std;

const int n_dim = 4;
const int m_dim = 3;


void print_table_result(int a[n_dim][m_dim], int rownum, int width, int colums, int row_found, int column_found)
{
	// border : 10
	// text   : 15

	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(h, 10);

	for (int i = 0; i <= rownum; i++)
	{
		cout << "|";
		for (int k = 1; k <= colums; k++)
		{
			for (int j = 1; j <= width; j++)
			{
				cout << "-";
			}

			cout << "|";
		}
		cout << endl;

		if (i != rownum)
		{
			cout << "|";

			for (int k = 0; k < colums; k++)
			{
				if (i == row_found && k == column_found)
				{
					SetConsoleTextAttribute(h, 14);
					cout << setw(width) << a[i][k];
					SetConsoleTextAttribute(h, 10);
				}
				else
				{
					SetConsoleTextAttribute(h, 15);
					cout << setw(width) << a[i][k];
					SetConsoleTextAttribute(h, 10);
				}

				cout << "|";
			}
			cout << endl;
		}
	}
	SetConsoleTextAttribute(h, 10);
}


void print_table(int a[n_dim][m_dim], int rownum, int width, int colums)
{
	// outer border : 14
	// inner border : 10
	// text         : 15

	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(h, 10);

	for (int i = 0; i <= rownum; i++)
	{
		cout << "|";
		for (int k = 1; k <= colums; k++)
		{
			for (int j = 1; j <= width; j++)
			{
				cout << "-";
			}

			cout << "|";
		}
		cout << endl;

		if (i != rownum)
		{
			cout << "|";

			for (int k = 0; k < colums; k++)
			{
				SetConsoleTextAttribute(h, 15);
				cout << setw(width) << a[i][k];
				SetConsoleTextAttribute(h, 10);

				cout << "|";
			}
			cout << endl;
		}
	}
}

void enter_array(int a[n_dim][m_dim])
{
	for (int i = 0; i < n_dim; i++)
		for (int j = 0; j < m_dim; j++)
		{
			cout << "a[" << i << "][" << j << "] : ";
			cin >> a[i][j];
		}
}

bool find_element(int a[n_dim][m_dim], int element, int &row_found, int &column_found)
{
	for (int i = 0; i < n_dim; i++)
		for (int j = 0; j < m_dim ; j++)
			if (a[i][j] == element)
			{
				row_found = i;
				column_found = j;
				return 1;
			}
	return 0;
}

void main(int argc, _TCHAR* argv[])
{
	int w;
	cout << "Width : ";
	cin >> w;

	int arr[n_dim][m_dim];

	enter_array(arr);

	print_table(arr, n_dim, w, m_dim);

	cout << "Enter Element to find \n";
	int el;
	cin >> el;

	int row_found = -1, column_found = -1;
	bool result = find_element(arr, el, row_found, column_found);

	if (result == 1)
	{
		cout << "Found in row : " << row_found << " and column : " << column_found << endl;
	}
	else
		cout << "Not yet\n";

	print_table_result(arr, n_dim, w, m_dim, row_found, column_found);
}

