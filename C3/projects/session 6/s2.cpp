
#include "stdafx.h"
#include <iomanip>
#include <string>
#include <iostream>
#include <Windows.h>	
using namespace std;

void print_table(int rownum, int width, int colums)
{
	// outer border : 14
	// inner border : 10
	// text         : 15

	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(h, 10); 

	for (int i = 0; i <= rownum; i++)
	{
		cout << "|";
		for (int k = 1; k <= colums; k++)
		{
			for (int j = 1; j <= width; j++)
			{
				cout << "-";
			}
			
			cout << "|";
		}
		cout << endl; 

		if (i != rownum)
		{
			cout << "|";

			for (int k = 1; k <= colums; k++)
			{
				SetConsoleTextAttribute(h, 15);
				cout <<setw(width) << "hi";
				SetConsoleTextAttribute(h, 10);

				cout << "|";
			}
			cout << endl;
		}
	}
}

void enter_array(int a[4][3] , int n , int m)
{
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
		{
		cout << "a[" << i << "][" << j << "] : ";
		cin >> a[i][j];
		}
}

void main(int argc, _TCHAR* argv[])
{

	/*
	int r, w, c;

	cout << "Rows : ";
	cin >> r;
	cout << "Width : ";
	cin >> w;
	cout << "Columns : ";
	cin >> c;
	*/

	const int r1 = 4, w1 = 8, c1 = 3;
	int arr[r1][c1];

	enter_array(arr , r1 , c1);
	print_table(r1, w1, c1);
}

