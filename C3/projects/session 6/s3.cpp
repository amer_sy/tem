
#include "stdafx.h"
#include <iomanip>
#include <string>
#include <iostream>
#include <Windows.h>	
using namespace std;

const int n_dim = 4;
const int m_dim = 3;

void print_table(int a[n_dim][m_dim] , int rownum, int width, int colums)
{
	// outer border : 14
	// inner border : 10
	// text         : 15

	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(h, 10); 

	for (int i = 0; i <= rownum; i++)
	{
		cout << "|";
		for (int k = 1; k <= colums; k++)
		{
			for (int j = 1; j <= width; j++)
			{
				cout << "-";
			}
			
			cout << "|";
		}
		cout << endl; 

		if (i != rownum)
		{
			cout << "|";

			for (int k = 0; k < colums; k++)
			{
				SetConsoleTextAttribute(h, 15);
				cout <<setw(width) << a[i][k];
				SetConsoleTextAttribute(h, 10);

				cout << "|";
			}
			cout << endl;
		}
	}
}

void enter_array(int a[n_dim][m_dim])
{
	for (int i = 0; i < n_dim; i++)
		for (int j = 0; j < m_dim; j++)
		{
			cout << "a[" << i << "][" << j << "] : ";
			cin >> a[i][j];
		}
}

void main(int argc, _TCHAR* argv[])
{
	int w;
	cout << "Width : ";
	cin >> w;

	int arr[n_dim][m_dim];

	enter_array(arr);

	print_table(arr , n_dim, w, m_dim);
}

