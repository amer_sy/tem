﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void readArray(int[] a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                Console.Write("enter element {0}:  ", i + 1);
                int.TryParse(Console.ReadLine(), out a[i]);
            }
            Console.WriteLine();
        }

        static void writeArray(int[] a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                Console.Write("{0}\t",a[i]);
            }
            Console.WriteLine();
        }

        static void arraySum(int[] a, int[] b, int[] c)
        {
            for (int i = 0; i < a.Length; i++)
            {
                c[i] = a[i] + b[i];
            }
            Console.WriteLine();
        }

        static void writeArray(int[] a, int[] b, int[] c)
        {
            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}",a[i],b[i],c[i]);
            }
            Console.WriteLine();
        }

        static double avr(int[] a)
        {
            double sum = 0;
            for (int i = 0; i < a.Length; i++)
            {
                sum += a[i];
            }
            return 1.0 * sum / a.Length;
        }

        static double avrPass(int[] a)
        {
            double sum = 0;
            int n = 0;
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] >= 60)
                {
                    sum += a[i];
                    n++;
                }
            }
            return 1.0 * sum / n;
        }

        static double avrFailed(int[] a)
        {
            double sum = 0;
            int n = 0;
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] < 60)
                {
                    sum += a[i];
                    n++;
                }
            }
            return 1.0 * sum / n;
        }
        static void writeArrayPass(int[] a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine("element {0}:  {1}", i + 1, a[i]);
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            int n = 3;
            int[] a = new int[n];
            int[] b = new int[n];
            int[] c = new int[n];

            readArray(a);
            readArray(b);

            arraySum(a, b, c);

            //writeArray(a);
            //Console.WriteLine("+");
            //writeArray(b);
            //Console.WriteLine("=");
            //writeArray(c);


            Console.WriteLine("A  +  \tB   = \tC");
            Console.WriteLine("-------------------------");
            writeArray(a, b, c);
            
            Console.ReadKey();
        }
    }
}
