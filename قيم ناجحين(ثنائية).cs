﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication16
{
    class Program
    {
        static void array1(int[,] b)
        {
            for (int i = 0; i < b.GetLength(0); i++)
            {
                for (int j = 0; j < b.GetLength(1); j++)
                {
                    Console.WriteLine("enter element{0}: ",i);
                    int.TryParse(Console.ReadLine(), out b[i,j]);
                }
            }
            Console.WriteLine();
        }
        static void pass1(int[,] b)
        {
            for (int i = 0; i < b.GetLength(0); i++)
            {
                for (int j = 0; j < b.GetLength(1); j++)
                {
                    if (b[i,j] > 60)
                    {
                        Console.Write("({0})\t", b[i,j]);
                    }

                    else
                    {
                        Console.Write("{0}\t",b[i,j]);
                    }
                 
                }

                Console.WriteLine();
            }
        }
        static void Main(string[] args)
        {
             int[,] b = new int[4,4];
            array1(b);
            pass1(b);
            Console.ReadKey();
        }
    }
}
