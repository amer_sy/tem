﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication21
{
    class laptop
    {
        public string name;
        public string model;
        public string Processor;
        public int Ramat;
        public double Monitor;
        public int price;
        public laptop(string n = "hp", string mod = "15-bs032ne", int ram = 4, double mon = 15.6, int p = 300000)
        {
            name = n;
            model = mod;
            Ramat = ram;
            Monitor = mon;
            price = p;
        }
        public void write1()
        {
            Console.WriteLine("{0} {1}", model, name);
            Console.WriteLine(Processor);
            Console.WriteLine(Ramat);
            Console.WriteLine(Monitor);
            Console.WriteLine(price);
        }
    }

    class mobile
    {
        public string name;
        public string model;
        public string Processor;
        public int Ramat;
        public double Monitor;
        public int price;
        public string OS;
        public int battery;

        public void read()
        {
            name = Console.ReadLine();
            model = Console.ReadLine();
            Processor = Console.ReadLine();
            OS = Console.ReadLine();
            int.TryParse(Console.ReadLine(), out Ramat);
            double.TryParse(Console.ReadLine(), out Monitor);
            int.TryParse(Console.ReadLine(), out price);
            int.TryParse(Console.ReadLine(), out battery);
        }
        public void write2()
        {
            Console.WriteLine("{0} {1}", name, model);
            Console.WriteLine(Processor);
            Console.WriteLine(OS);
            Console.WriteLine(Ramat);
            Console.WriteLine(Monitor);
            Console.WriteLine(price);
            Console.WriteLine(battery);
        }
    }

    class fan
    {
        public string Type;
        public int FanBlades;
        public int Size;
        public string color;
        public int theSpeed;
        public int AirFlowRate;
        public string OperatingVoltage;
        public int daman;

        public fan()
        {
            Type = "sakf";
           FanBlades = 3;
           Size = 56;
           color = "black";
           theSpeed = 270;
           AirFlowRate = 18000;
           OperatingVoltage = "220 foalt - 50 minet";
           daman = 5;
        }
        public void write3()
        {
            Console.WriteLine(Type);
            Console.WriteLine(FanBlades);
            Console.WriteLine(Size);
            Console.WriteLine(color);
            Console.WriteLine(theSpeed);
            Console.WriteLine(AirFlowRate);
            Console.WriteLine(OperatingVoltage);
            Console.WriteLine(daman);
        }
    }
    class Program
    {

        static void Main(string[] args)
        {
            laptop x = new laptop();
            x.write1();
            mobile y = new mobile();
            y.read();
            y.write2();
            fan z = new fan();
            z.write3();
            Console.ReadKey();
        }
    }
}
