﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Student
    {
        private int by;

        public int birthdayYear
        {
            get
            {
                return by;
            }
            set
            {
                if (value <= 2018 && value >= 1918)
                    by = value;
            }
        }
        public int age
        {
            get
            {
                return 2018 - by;
            }

            set // value
            {
                if (value >= 0 && value <= 100)
                    by = 2018 - value;
            }
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            Student x = new Student();
            x.birthdayYear = 20000;
            Console.WriteLine(x.birthdayYear);
            Console.WriteLine(x.age);
            x.age = 0;
            Console.WriteLine(x.birthdayYear);
            Console.WriteLine(x.age);
            Console.ReadKey();

        }
    }
}
